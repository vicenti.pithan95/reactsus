import React, { useState, useEffect } from "react";
import { Container, Typography, Button } from '@material-ui/core';
import api from "../../service/api"
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { useHistory } from "react-router-dom";

const useStylesBt ={
  margin: "1% 1%",
}

const useStyles ={
  margin: "5% 0",
}
function ListaCidadaos() {
  const history = useHistory();
  const [cidadaos, setCidadaos] = useState([]);
  useEffect(() => {
    api.listaCidadaos().then((e) => {
      let cidadao;
      let list = []
      e.forEach(element => {
      let grupo = ((element.grupoEspecial.nmGrupo) + "/" + (element.grupoEspecial.isComorbidade ? "comorbidade" : "Grupo prioritário"));
       let data = new Date(element.dtNascimento);
        cidadao = {
          nmCidadao: element.nmCidadao,
          email: element.email,
          isNotificado: element.isNotificado ? "enviado" : "Não enviado",
          isNotificadoBot: element.isNotificadoBot ? "enviado" : "Não enviado",
          dtNascimento: ((data.getDate() )) + "/" + ((data.getMonth() + 1)) + "/" + data.getFullYear(),
          cidade: ((element.local.nmCidade) + "/" + (element.local.siglaUf)),
          grupo: (element.grupoEspecial.nmGrupo ==="NAO"? "NAO" : grupo ),
        }
        list.push(cidadao);
      });
      console.log(list);
      setCidadaos(list);
    });
  }, []);
  return (
    <Container>
      <div style={useStyles}></div>
      <Typography height="auto" align="center" variant="h5">
        Lista de cidadãos cadastrados!
      </Typography>
      <TableContainer component={Paper}>
        <Table size="medium" aria-label="a dense table">
          <TableHead>
            <TableRow>
              <TableCell>Nome:</TableCell>
              <TableCell align="right">email</TableCell>
              <TableCell align="right">Notificação email</TableCell>
              <TableCell align="right">Notificação Telegram</TableCell>
              <TableCell align="right">Data de nascimento</TableCell>
              <TableCell align="right">Cidade</TableCell>
              <TableCell align="right">Grupo de Vac</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {cidadaos.map((cidadao) => (
              <TableRow key={cidadao.id}>
                <TableCell component="th" scope="row">
                  {cidadao.nmCidadao}
                </TableCell>
                <TableCell align="right">{cidadao.email}</TableCell>
                <TableCell align="right">{cidadao.isNotificado}</TableCell>
                <TableCell align="right">{cidadao.isNotificadoBot}</TableCell>
                <TableCell align="right">{cidadao.dtNascimento}</TableCell>
                <TableCell align="right">{cidadao.cidade}</TableCell>
                <TableCell align="right">{cidadao.grupo}</TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
      <Button
        style={useStylesBt}
        variant="contained"
        color="primary"
        onClick={() => {
          history.push("/menu");
        }}
      >
        voltar
      </Button>
    </Container>
  );
}

export default ListaCidadaos;
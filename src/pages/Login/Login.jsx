import React, { useState, useEffect } from "react";
import { useValidacoesCadastro }  from "../../contexts/validacoesCadastro";
import {Button, TextField, Container, Typography } from '@material-ui/core';
import useErros from "../../hooks/useErros.js";
import { useHistory } from "react-router-dom";
import {Alert} from '@material-ui/lab';
import api from "../../service/api"
const useStyles ={
  margin: "1% 1%",
}
function Login() {
  const history = useHistory();
  const [nome, setNome] = useState("");
  const [senha, setSenha] = useState("");
  const validacoes = useValidacoesCadastro();
  const [erros, validarCampos] = useErros(validacoes);
  const [mensagemAlerta, setMensagemAlerta] = useState("error");
  const [tipoAlerta, setTipoAlerta] = useState("error");
  const [visivel, setVisivel] = useState(false);
  useEffect(() => {
    setTimeout(() => {
      setVisivel(false);
    }, 3000);
  }, [visivel]);
  function possoEnviar() {
    for (let campo in erros) {
      if (!erros[campo].valido) {
        return false;
      }
    }
    const senhaDecode = new Buffer(senha).toString('base64')
    const data = {
      username: nome,
      password: senhaDecode
    }
    api.login(data).then(success =>{
         if(success){
           history.push("/menu")
         }
         setVisivel(true);
         setTipoAlerta("error");
         setMensagemAlerta("Usuário e/ou senha inválidos!");
        })
    }
  return (
    <Container component="article" maxWidth="sm">
      <div style={useStyles}></div>
        {visivel && (
          <Alert severity={tipoAlerta} style={{ marginBottom: "30px" }}>
            {mensagemAlerta}
          </Alert>
        )}
      <Typography height="auto" align="center" variant="h5">
        Menu Administrativo
      </Typography>
      <form
        onSubmit={(event) => {
          event.preventDefault();
         possoEnviar()
        }}
      >
        <TextField
          value={nome}
          onChange={(event) => {
            setNome(event.target.value);
          }}
          id="nome"
          name="nome"
          label="nome"
          type="text"
          variant="outlined"
          margin="normal"
          fullWidth
          required
        />
        <TextField
          value={senha}
          onChange={(event) => {
            setSenha(event.target.value);
          }}
          onBlur={validarCampos}
          error={!erros.senha.valido}
          helperText={erros.senha.texto}
          id="senha"
          name="senha"
          label="senha"
          type="password"
          variant="outlined"
          margin="normal"
          fullWidth
          required
        />
        <Button type="submit" variant="contained" color="primary">
          Acessar
        </Button>
          <Button
            style= {useStyles}
            variant="contained"
            color="primary"
            onClick={() => {
              history.push("/");
            }}
          >
            voltar
          </Button>
      </form>
    </Container>
  );
}

export default Login;
import React  from "react";
import { Container, Button } from "@material-ui/core";
import logo from "../../img/logo.png"
import { useHistory } from "react-router-dom";
const useStyles ={
     margin: "1% 0%",
  }
const useStylesBt = {
  margin: "1% 0%",
};
function Menu() {
    const history = useHistory();
    return (
      <>
        <Container component="article" maxWidth="sm">
          <div className="logo">
            <img src={logo} alt="logo" align="center" height="265px"></img>
          </div>
          <Button
            style= {useStyles}
            variant="contained"
            color="primary"
            fullWidth
            margin
            onClick={() => {
              history.push("/menu/local");
            }}
          >
            Adicionar Local
          </Button>
          <Button
            style= {useStyles}
            variant="contained"
            color="primary"
            fullWidth
            onClick={() => {
              history.push("/menu/grupoEspecial");
            }}
          >
            Adicionar Grupo Especial
          </Button>
          <Button
            style= {useStyles}
            variant="contained"
            color="primary"
            fullWidth
            onClick={() => {
              history.push("/menu/vacinacao");
            }}
          >
            Adicionar Critério de Vacinação
          </Button>
          <Button
            style= {useStyles}
            variant="contained"
            color="primary"
            fullWidth
            onClick={() => {
              history.push("/menu/listaCidadaos");
            }}
          >
            Visualizar cidadãos
          </Button>
          <Button
          style={useStylesBt}
          variant="contained"
          color="primary"
          onClick={() => {
            history.push("/login");
          }}
        >
          voltar
        </Button>
        </Container>
      </>
    );
}



export default Menu;
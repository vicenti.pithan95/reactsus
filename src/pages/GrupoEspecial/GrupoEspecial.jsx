import React, { useState, useEffect } from "react";
import {Button, TextField, Container, Typography, FormControlLabel, Switch} from '@material-ui/core';
import { useHistory } from "react-router-dom";
import api from "../../service/api"
import {Alert} from '@material-ui/lab'

const useStyles ={
  margin: "15% 0",
}
const useStylesBt ={
  margin: "1% 1%",
}
function GrupoEspecial() {
  const history = useHistory();
  const [dsGrupoEspecial, setDsGrupoEspecial] = useState("");
  const [comorbidade, setComorbidade] = useState(true)
  const [mensagemAlerta, setMensagemAlerta] = useState("error");
  const [tipoAlerta, setTipoAlerta] = useState("error");
  const [visivel, setVisivel] = useState(false);
  useEffect(() => {
    setTimeout(() => {
      setVisivel(false);
    }, 3000);
  }, [visivel]);
  return (
    <Container component="article" maxWidth="sm">
      <div style={useStyles}></div>
      {visivel && (
        <Alert severity={tipoAlerta} style={{ marginBottom: "30px" }}>
          {mensagemAlerta}
        </Alert>
      )}
      <Typography height="auto" align="center" variant="h5">
        Adicionar Grupo Especial
      </Typography>
      <form
        onSubmit={(event) => {

          const dados = {
            nmGrupo: dsGrupoEspecial,
            isComorbidade: comorbidade
          };
          event.preventDefault();
          async function saving() {
           let resp =  await api.createGrupoEspecial(dados);
           return resp
          }
          saving().then((resp) => {
            console.log("no if o save é " + resp);
            setVisivel(true);
            if (resp) {
              setTipoAlerta("success");
              setMensagemAlerta("Grupo Especial salvo com sucesso !");
            } else {
              setTipoAlerta("error");
              setMensagemAlerta("Grupo Especial já cadatrado!");
            }
          });
          
          console.log({ dsGrupoEspecial, comorbidade });
        }}
      >
        <TextField
          value={dsGrupoEspecial}
          onChange={(event) => {
            setDsGrupoEspecial(event.target.value);
          }}
          id="gp"
          name="gp"
          label="Grupo Especial"
          variant="outlined"
          margin="normal"
          fullWidth
          required
        />
        <FormControlLabel
          label="comorbidade"
          control={
            <Switch
              onChange={(event) => {
                setComorbidade(event.target.checked);
              }}
              name="comorbidade"
              checked={comorbidade}
              color="primary"
            />
          }
        />

        <Button type="submit" variant="contained" color="primary">
          Adicionar
        </Button>
        <Button
          style={useStylesBt}
          variant="contained"
          color="primary"
          onClick={() => {
            history.push("/menu");
          }}
        >
          voltar
        </Button>
      </form>
    </Container>
  );
}

export default GrupoEspecial;
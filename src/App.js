import React from "react"
import "./App.css"
import { CreatedUserProvider } from "./contexts/createdUser"
import Routes from "./routes"

export default function App() {
  return (
    <CreatedUserProvider>
      <Routes/>
    </CreatedUserProvider>
  )
}

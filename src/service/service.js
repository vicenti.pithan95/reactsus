
import api from "./api";
export default async function cadastraUsuario(dados){
    console.table(dados)
    let grupo = dados.comorbidadeSelecionada ? dados.comorbidadeSelecionada : dados.grupoEspecialSelecionado;
    const data = {
      nmCidadao: dados.nome,
      dtNascimento: dados.nascimento + "T00:00:00",
      email: dados.email,
      primeiraDose: "2021-01-01T00:00:00",
      segundaDose: "2021-01-01T00:00:00",
      local: {
        id: dados.cidadeSelecionada
      },
      grupoEspecial: {
        id: grupo
      },
      vacina: "teste",
      isNotificado: false
    };      
    console.log(data);
    return api.save(data);
  }

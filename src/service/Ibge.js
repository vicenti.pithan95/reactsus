function cidadesRs() {
  return fetch(
    "https://servicodados.ibge.gov.br/api/v1/localidades/estados/43/municipios",
    {
      method: "GET",
      mode: "cors",
      credentials: "omit",
    }
  ).then((response) =>{
    let teste = response.json();
    return teste.then(cidades=>{
      return cidades.map(({ id, nome }) => ({
        id,
        nome,
      }));
    })
  });
}
export default cidadesRs;

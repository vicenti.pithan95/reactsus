import React from 'react';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Home from './pages/Home/Home';
import Login from './pages/Login/Login';
import Local from './pages/Local/Local';
import GrupoEspecial from './pages/GrupoEspecial/GrupoEspecial';
import Vacinacao from './pages/Vacinacao/Vacinacao';
import Menu from './pages/Menu/Menu';
import PrivateRoute from './PrivateRoute';
import ListaCidadaos from './pages/ListaCidadaos/ListaCidadadaos'
function Routes() {
  return (
  <Router>
    <Switch>
        <Route exact path="/" component={Home}/>
        <Route exact path="/login" component={Login}/>
        <PrivateRoute exact path="/menu" component={Menu}/>
        <PrivateRoute exact path="/menu/local" component={Local}/>
        <PrivateRoute exact path="/menu/grupoEspecial" component={GrupoEspecial}/>
        <PrivateRoute exact path="/menu/vacinacao" component={Vacinacao}/>
        <PrivateRoute exact path="/menu/listaCidadaos" component={ListaCidadaos}/>
    </Switch>
  </Router>
  );
}

export default Routes;
import React, { useState, useEffect } from "react";
import {Button, TextField, MenuItem } from '@material-ui/core';
import api from "../../service/api.js";
import { useUserContext } from "../../contexts/createdUser";
import cadastraUsuario from "../../service/service.js"

function DadosTriagem({proximo}) {
  const [dados, setDados] = useState({
    estado: "Rio Grande do Sul",
    cidadeSelecionada: "",
    comorbidadeSelecionada: 0,
    grupoEspecialSelecionado: 0
  });
  const [estado, setEstado] = useState("Rio Grande do Sul");
  const [cidades, setCidades] = useState([]);
  const [comorbidade, setComorbidade] = useState([]);
  const [grupoEspecial, setGrupoEspecial] = useState([]);
  
  const { user } = useUserContext();
  
  useEffect(() => {
    const buscarCampos = async () => {
      await Promise.all([api.local().then(e=>{
        setCidades(e)
      }),
      api.grupo().then(e=>{
        setGrupoEspecial(e)
      }),
      api.comorbidade().then(e=>{
        setComorbidade(e)
      })]);
    }
    buscarCampos();
  }, []);


  const handleChange = (event) => {
    const { name, value } = event.target
    setDados({...dados, [name]: value})
  };

  return (
    <form
      onSubmit={async (event) => {
        event.preventDefault();
        cadastraUsuario({ ...user, ...dados }).then((resp) => {
          console.log(resp);
          proximo();
        });
      }}
    >
      <TextField
        value={estado}
        onChange={(event) => {
          setEstado(event.target.value);
        }}
        id="estado"
        name="estado"
        label="Estado"
        type="text"
        variant="outlined"
        margin="normal"
        fullWidth
        disabled={true}
      />

      <TextField
        variant="outlined"
        margin="normal"
        fullWidth
        name="grupoEspecialSelecionado"
        select
        value={dados.grupoEspecialSelecionado}
        onChange={handleChange}
        label="Grupo especial"
      >
        {grupoEspecial.map((item) => (
          <MenuItem key={item.id} value={item.id}>
            {item.nmGrupo}
          </MenuItem>
        ))}
      </TextField>
      <TextField
        variant="outlined"
        margin="normal"
        fullWidth
        select
        name="comorbidadeSelecionada"
        value={dados.comorbidadeSelecionada}
        onChange={handleChange}
        label="Comorbidade"
      >
        <MenuItem key={0} value={0}>
          Não
        </MenuItem>
        {comorbidade.map((item) => (
          <MenuItem key={item.id} value={item.id}>
            {item.nmGrupo}
          </MenuItem>
        ))}
      </TextField>

      <TextField
        variant="outlined"
        margin="normal"
        fullWidth
        select
        name="cidadeSelecionada"
        value={dados.cidadeSelecionada}
        onChange={handleChange}
        label="Cidade"
      >
        {cidades.map((cidade) => (
          <MenuItem key={cidade.id} value={cidade.id}>
            {cidade.nmCidade}
          </MenuItem>
        ))}
      </TextField>

      <Button
        type="submit"
        margin="normal"
        variant="contained"
        color="primary"
        fullWidth
      >
        Finalizar Cadastro
      </Button>
    </form>
  );
}
export default DadosTriagem;
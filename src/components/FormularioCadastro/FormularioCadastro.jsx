import React, { useState } from "react";
import DadosPessoais from "./DadosPessoais";
import DadosTriagem from "./DadosTriagem";
import Resposta from "./Resposta";
import {  Stepper,Step, StepLabel } from "@material-ui/core";
 
function FormularioCadastro(){
  const [etapaAtual, setEtapaAtual] = useState(0);

  const formularios = [
    <DadosPessoais proximo={proximo}/>,
    <DadosTriagem proximo={proximo}/>,
    <Resposta />,
  ];

  function proximo() {
    setEtapaAtual(etapaAtual+1);
  }

  return (
    <>
      <Stepper activeStep={etapaAtual}>
        <Step>
          <StepLabel>Cadastro</StepLabel>
        </Step>
        <Step>
          <StepLabel>Triagem</StepLabel>
        </Step>
        <Step>
          <StepLabel>Resultado</StepLabel>
        </Step>
      </Stepper>
      {formularios[etapaAtual]}
    </>
  );
}

export default FormularioCadastro;
